const paramTemplate = '<input class="q-input" placeholder="Param Key"/>';
const queryTemplate =
  '<input class="q-input" placeholder="Query Key"/><input class="q-input" placeholder="Query Value"/>';
const baseUrl = "https://website.ir";

const urlContainer = document.getElementById("url-container");
const paramContainer = document.getElementById("params-container");
const queryContainer = document.getElementById("queries-container");

let params = [];
let queries = {};

const isEmpty = (value) => {
  return value === "" && !value && !value.trim();
};

const createBox = () => {
  const container = document.createElement("div");
  container.setAttribute("class", "keyValue-box");
  return container;
};

const addNewParam = () => {
  const input = document.querySelector(
    "#params-container .keyValue-box:last-child .q-input"
  );
  const paramValue = input.value;

  if (isEmpty(paramValue)) {
    alert("please enter param value!");
    return;
  }

  const paramBox = createBox();
  paramBox.insertAdjacentHTML("beforeend", paramTemplate);

  paramContainer.append(paramBox);

  params.push(paramValue);
};

const addNewQuery = () => {
  const queryInputs = document.querySelectorAll(
    "#queries-container .keyValue-box:last-child .q-input"
  );

  const key = queryInputs[0].value;
  const value = queryInputs[1].value;

  if (isEmpty(key) || isEmpty(value)) {
    alert("please enter query key and value!");
    return;
  }

  queries = {
    ...queries,
    [key]: value,
  };

  const queryBox = createBox();
  queryBox.insertAdjacentHTML("beforeend", queryTemplate);

  queryContainer.append(queryBox);
};

const generateURL = () => {
  urlContainer.innerHTML = "";

  const urlParams = params.join("/");

  const queryArray = Object.entries(queries);
  const queryString = new URLSearchParams(queryArray).toString();

  const url = new URL(baseUrl);
  url.href = `${url.href}${urlParams}`;
  url.search = queryString;

  renderUrl(url.toString());
};

const renderUrl = (url) => {
  const urlElm = document.createElement("p");
  urlElm.style.backgroundColor = "blue";
  urlElm.style.color = "#fff";
  urlElm.innerHTML = `"${url}"`;
  urlContainer.append(urlElm);
};

document.getElementById("param-submit").addEventListener("click", addNewParam);
document.getElementById("query-submit").addEventListener("click", addNewQuery);
document.getElementById("generate").addEventListener("click", generateURL);
